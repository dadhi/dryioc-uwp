﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests4InlineBuildTaskSnippets
{
    [TestClass]
    public class NuGetTasksTest
    {
        [TestMethod]
        public void NuGet_GetCommandLineToolPathTest()
        {
            // Task params
            string SolutionDir = @"C:\e-Soft\DryIoC-UWP\"; // Input
            string FilePath = null; // Output

            // Task body

            // Test solution wide packages repository (if exists)
            string directoryToStartWith = SolutionDir + "packages";
            if (Directory.Exists(directoryToStartWith))
            {
                // Look for NuGet.CommandLine package directory
                var dirs = Directory.GetDirectories(directoryToStartWith, "NuGet.CommandLine.*");
                if (dirs != null && dirs.Length > 0)
                {
                    string NuGet_CommandLine_Package_Directory = dirs[dirs.Length - 1];

                    // Look for NuGet.exe file
                    var files = Directory.GetFiles(NuGet_CommandLine_Package_Directory, "NuGet.exe", System.IO.SearchOption.AllDirectories);
                    FilePath = files[files.Length - 1];
                }
            }

            // If NuGet.exe doesn't exist in solution packages directory - try to look for it in global repository in %userprofile%\.nuget\packages
            if (FilePath == null)
            {
                string globalRepositoryFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\.nuget\packages";
                if (Directory.Exists(globalRepositoryFolder))
                {
                    // Look for NuGet.CommandLine package directory
                    var dirs = Directory.GetDirectories(globalRepositoryFolder, "NuGet.CommandLine.*");
                    if (dirs != null && dirs.Length > 0)
                    {
                        string NuGet_CommandLine_Package_Directory = dirs[dirs.Length - 1];

                        // Look for NuGet.exe file
                        var files = Directory.GetFiles(NuGet_CommandLine_Package_Directory, "NuGet.exe", System.IO.SearchOption.AllDirectories);
                        FilePath = files[files.Length - 1];
                    }
                }
            }
        }
    }
}
