﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests4InlineBuildTaskSnippets
{
    [TestClass]
    public class NuSpecTaskTest
    {
        [TestMethod]
        public void ReplaceVersionInfoTest()
        {
            // Input params
            string VersionNumber = "1.0.0.2";
            string VersionPostfix = "beta";
            string NuspecFilePath = @"C:\e-Soft\DryIoC-UWP\NuGet.AsContent\DryIoc.AsContent.nuspec";

            // Task body

            // Construct version string
            string versionString = VersionNumber;
            if (!string.IsNullOrWhiteSpace(VersionPostfix))
                versionString += "-" + VersionPostfix;

            // Load nuspec file
            System.Xml.Linq.XDocument nuspecDoc = System.Xml.Linq.XDocument.Load(NuspecFilePath);

            // Modify version element
            nuspecDoc.Element("package").Element("metadata").Element("version").Value = versionString;

            // Store updates
            nuspecDoc.Save(NuspecFilePath);
        }
    }
}
